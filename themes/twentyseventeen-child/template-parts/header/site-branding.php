<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

function fancy_header($str) {
    $chars = str_split($str);
    $len = count($chars);
    $str_out = '';
    foreach($chars as $key => $char) {
        $colour = ($key % 6);
        $key = ($key % 5) + 1;
        
        $str_out .= '<span class="tag_font_3'
            . ' tag_font_colour_' . $colour . "\">$char</span>";
    }
    return $str_out;
}
$site_name = get_bloginfo( 'name' );

?>
<div class="site-branding">
    <div class="wrap">

        <?php the_custom_logo(); ?>

        <div class="site-branding-text">
            <?php if ( is_front_page() ) : ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo fancy_header($site_name); ?></a></h1>
            <?php else : ?>
                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo fancy_header($site_name); ?></a></p>
            <?php endif; ?>

            <?php
            $description = get_bloginfo( 'description', 'display' );

            if ( $description || is_customize_preview() ) :
            ?>
                <p class="site-description"><?php echo $description; ?></p>
            <?php endif; ?>
        </div><!-- .site-branding-text -->

        <?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && ! has_nav_menu( 'top' ) ) : ?>
        <a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
    <?php endif; ?>

    </div><!-- .wrap -->
</div><!-- .site-branding -->
