<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$tag_cloud_public = get_query_var('tag_cloud_public');
$count = $tag_cloud_public->post_count();
$pricing = $tag_cloud_public->get_pricing();
$premium = $tag_cloud_public->get_toggle();
/*
Template Name: Register Page
 
@refer https://millionclues.com/tutorials/custom-wordpress-register-login-page
*/
 
// Exit if accessed directly

if ( !defined('ABSPATH')) exit;
get_header();?>



<div class="wrap">

<form id="register_tag" class="form_registration form-hide_" method="post" enctype="multipart/form-data" action="<?php echo admin_url( 'admin-post.php' );?>" novalidate>
<h1 id="sold_count"></h1>
<div class="form-input">
    <input type="hidden" name="action" value="process_form">
    
    <div>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email" required  autocomplete="email" />
        <div class="invalid-feedback is-invalid">
            Please enter a valid email address
        </div>
    </div>
    
    <div class="form-group">
        <input type="text" maxlength="100" pattern="^(?:(?![_])[\u0020-\u007f\u002a-\u00ff])+$" class="form-control" name="tag" id="tag"  placeholder="Slogan" required autocomplete="off" />
        <div class="input-slogan-count">100</div>
        <div class="invalid-feedback is-invalid">
            Only letters and numbers are allowed
        </div>
    </div>
    
    <div>
        <input type="url" class="form-control" name="url" id="url"  placeholder="URL"  autocomplete="off" />
        <div class="invalid-feedback is-invalid">
            Please enter a valid URL
        </div>
    </div>

    <div id="font-options" class="options-hide">
        <label id="check_bold_label">Bold
            <input type="checkbox" id="font_check_bold" name="font_check_bold" />
        </label>
    
        <select class="form-control" name="font_size" id="font_size" autocomplete="off">
            <?php
            foreach ($tag_cloud_public->options['options_premium']['font_sizes'] as $key => $font_size) {
                $selected = '';
                if ($key === 0) {
                    $selected = ' selected="selected"';
                }
                ?>
                <option value="<?php echo $key;?>"<?php echo $selected;?>><?php echo $font_size;?></optiom>
                <?php
            }
            ?>
        </select>
    </div>
    <select class="form-control" name="font_colour" id="font_colour" autocomplete="off">
        <?php
        foreach ($tag_cloud_public->options['options_premium']['font_colours'] as $key => $font_colour) {
        ?>
            <option value="<?php echo $key;?>"><?php echo $font_colour;?></optiom>
        <?php
        }
        ?>
    </select>
    <div id="swatch-container">
    <?php
    foreach ($tag_cloud_public->options['options_premium']['font_colours'] as $key => $font_colour) {
        ?>
        <div class="swatch-outer">
            <div class="swatch tag_font_bg_<?php echo $key;?>"data-key="<?php echo $key;?>" data-colour="<?php echo $font_colour;?>"></div>
        </div>
        <?php
    }
    ?>
    </div>


<!--<div class="form-group tag_preview">
    <div class="card">
        <div id="slogan-preview" class="tag_font_1 tag_font_size_0 tag_font_colour_0">Your slogan</div>
        <span class="slogan-click">(click to shuffle)</span>
    </div>
</div>-->

<div id="slogan-preview" class="tag_font_1 tag_font_size_0 tag_font_colour_0"></div>




<!--<div class="form-group">
        <label for="colour" class="cols-sm-2 control-label">Colour</label><br>
        <input type="color" name="colour" value="#9b59b6" class="custom">
</div>-->

<?php
if ($premium) {
?>
<div id="price"></div>
<div id="tag-paypal-button"></div>
<?php
}
else {
?>
<button type="submit" id="tag-submit" class="submit-show">Submit</button>
<?php
}
?>
<div style="clear: both;"></div>


</div>
</form>
<!--this is required to prevent theme js from erroring-->
<div id="secondary"></div>
</div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>



<script>

window.postCount = <?php echo $count;?>;

var premium = <?php echo $premium;?>;
<?php
if ($premium) {
?>
var pricing = <?php echo json_encode($pricing, JSON_PRETTY_PRINT);?>;
<?php
}
?>


</script>
<!--<script src="//console.re/connector.js" data-channel="f6b1-304a-9f45" id="consolerescript"></script>-->
<script>

</script>

<?php
get_footer();
