<?php

class Wp_Tagcloud_Admin {

    //[label, html input type]
    private $field_names = [
        'font_bold' => ['Bold', 'number'],
        'font' => ['Font', 'number'],
        'font_size' => ['Font size', 'number'],
        'font_colour' => ['Font colour', 'number'],
        'url' => ['URL', 'url'],
        'email' => ['Email', 'email'],
        'premium' => ['Premium', 'number'],
        'pp_id' => ['Paypal ID', 'text'],
        'pp_email' => ['Paypal email', 'text'],
        'pp_data' => ['Paypal response', 'text']
    ];

    private $option_name = 'wp_tagcloud';

    private $plugin_name;

    private $version;

    public function __construct( $plugin_name, $version, $options ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->options = $options;

    }

    public function enqueue_styles() {

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-tagcloud-admin.css', [], $this->version, 'all' );

    }

    public function enqueue_scripts() {

        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-tagcloud-admin.js', [ 'jquery' ], $this->version, false );

    }

    public function save_post($post_id, $post, $update){

        if ($post->post_type != 'cloudtags'){
            return;
        }
        if ($update === false) {
            return;
        }

        $post = get_post($post_id);

        foreach ($this->field_names as $field_name => $label) {

            if( !isset($_POST[$field_name]) ) continue;
        
            //check sanitation on this - to do
            $field_value = sanitize_text_field(trim($_POST[$field_name]));

            if( $field_value ) {
                update_post_meta($post_id, $field_name, $field_value);
            }
            else {
                delete_post_meta($post_id, $field_name);
            }
        }

    }

    public function cloudtags_meta(WP_Post $post) {

        add_meta_box('cloud_meta', 'Post meta', function() use ($post) {
            wp_nonce_field('cloud_nonce', 'cloud_nonce');
            foreach ($this->field_names as $field_name => $label) {
                $field_value = get_post_meta($post->ID, $field_name, true);
                ?>
                <label style="float: left; width: 120px;" for="<?php echo $field_name; ?>"><?php echo $label[0];?></label>
                <input id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" type="<?php echo $label[1];?>" value="<?php echo esc_attr($field_value); ?>" />
                <br />
                <?php
            }
        });

    }

    public function create_posttype() {

        register_post_type( 'cloudtags',
            [
                'register_meta_box_cb' => [$this, 'cloudtags_meta'],
                'labels' => [
                    'name' => 'Cloud Tags',
                    'singular_name' => 'Cloud Tag'
                ],
                'public' => true,
                'has_archive' => true,
                'rewrite' => ['slug' => 'cloudtags'],
            ]
        );
    }
    
    public function display_options_page() {
        include_once 'partials/wp-tagcloud-admin-display.php';
    }

    public function add_options_page() {

        $this->plugin_screen_hook_suffix = add_options_page(
            'Cloud Tags',
            'Cloud Tags',
            'manage_options',
            $this->plugin_name,
            [ $this, 'display_options_page' ]
        );
    
    }
    public function pre_update_option($value, $option_name, $old_value) {

        if (isset($_POST['wp_tagcloud_toggle']) && $_POST['wp_tagcloud_toggle'] === '0') {
            //no cleaner way to do this.  we don't want to reset to defaults on selection of 'free'
            if ($option_name !== 'wp_tagcloud_toggle') return $old_value;
        }
        return $value;

    }
    public function settings_sanitise_input($input_val, $step) {

        if ((int) $step == (float) $step) {
            //loose comparison, $step was int
            $cb = 'intval';
        }
        else {
            $cb = 'floatval';
        }
        return $cb($input_val);

    }
    public function add_settings_field_input($id, $title, $cb_suffix, $type, $step) {

        add_settings_field(
            $this->option_name . "_$id",
            $title,
            [ $this, $this->option_name . $cb_suffix ],
            $this->plugin_name,
            $this->option_name . '_general',
            [
                'label_for' => $this->option_name . "_$id",
                'type' => $type,
                'step' => $step
            ]
        );
        register_setting( $this->plugin_name, $this->option_name . "_$id", [
            'sanitize_callback' =>  function($wp_input_val) use ($step) {
                return $this->settings_sanitise_input($wp_input_val, $step);
            }
        ]);

    }
    public function add_settings_section() {

        add_settings_section(
            $this->option_name . '_general',
            'Settings',
            [ $this, $this->option_name . '_settings_section_cb' ],
            $this->plugin_name
        );
        $id = 'toggle';
        add_settings_field(
            $this->option_name . "_$id",
            'Premium',
            [ $this, $this->option_name . '_settings_' . $id . '_cb' ],
            $this->plugin_name,
            $this->option_name . '_general',
            [ 'name' => $this->option_name . "_$id" ]
        );
        
        register_setting( $this->plugin_name, $this->option_name . "_$id", [
            'sanitize_callback' => 'intval'
        ]);

        foreach ($this->options['fields_input'] as $key => $value) { 
            if ($value[0] === null) {
                //null at ind 0 implies nested (sizing options)
                foreach ($value as $key => $value) {
                    $this->add_settings_field_input($key, $value[0], '_settings_cb', $value[1], $value[2]);
                }
                continue;
            }
            $this->add_settings_field_input($key, $value[0], '_settings_cb', $value[1], $value[2]);
        }
    }

    public function wp_tagcloud_settings_section_cb() {
        echo '<h3>Pricing options</h3>';
    }
    public function wp_tagcloud_settings_toggle_cb($args) {
        $name = $args['name'];
        $selected = get_option($name);
        ?>
        <label>
            <input type="radio" class="tc_radio" name="<?php echo $this->option_name . '_toggle' ?>" value="1" <?php if ($selected) echo ' checked';?>>
            <?php echo 'Premium'; ?>
        </label>
        <br>
        <label>
            <input type="radio" class="tc_radio" name="<?php echo $this->option_name . '_toggle' ?>" value="0"<?php if (!$selected) echo ' checked';?>>
            <?php echo 'Free'; ?>
        </label>
        <?php
    }
    public function wp_tagcloud_settings_cb($args) {
        //this prints the output 
        $name = $args['label_for'];
        $type = $args['type'];
        $step = $args['step'];
        $value = get_option($name);
        echo "<input type=\"$type\" class=\"tc_input\" value=\"$value\" name=\"$name\" step=\"$step\" />";
    }
}
