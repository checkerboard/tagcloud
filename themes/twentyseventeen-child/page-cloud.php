<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if (isset($_GET['thankyou'])) {
    header('Location: /');
}

$tag_cloud_public = get_query_var('tag_cloud_public');

if ( !defined('ABSPATH')) exit;
get_header();
?>
<div class="wrap wrap-full">
<div id="wp_cloud">
<?php
$tag_cloud_public->display_cloud();
?>
</div>

<!--this is required to prevent theme js from erroring-->
<div id="secondary"></div>
</div>
<?php
get_footer();
