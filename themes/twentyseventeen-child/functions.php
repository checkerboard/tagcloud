<?php

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
 
function redirect_home( $original_template ) {
   if ( is_front_page() ) {
      return get_theme_file_path() . '/page-cloud.php';
   }
   else {
      return $original_template;
   }
}

add_filter( 'template_include', 'redirect_home' );


/*function redirect_login_page() {
    
  $register_page  = '/register';
  $login_page    = home_url( '/register?action=login' );
  $page_viewed   = basename($_SERVER['REQUEST_URI']);
 
  if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
    wp_redirect($login_page);
    exit;
  }
  
  if( $page_viewed == "wp-login.php?action=register" && $_SERVER['REQUEST_METHOD'] == 'GET') {
    wp_redirect($register_page);
    exit;
  }
}*/
//add_action('init','redirect_login_page');
 
// Redirect For Login Failed
function login_failed() {
  
  wp_redirect( home_url( '/register?action=login&login=failed' ) );
  exit;
}
add_action( 'wp_login_failed', 'login_failed' );
 
// Redirect For Empty Username Or Password
function verify_username_password( $user, $username, $password ) {
  if ( $username == "" || $password == "" ) {
    
    wp_redirect( home_url( '/register?action=login&login=empty' ) );
    exit;
  }
}
//add_filter( 'authenticate', 'verify_username_password', 1, 3);