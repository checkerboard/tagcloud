<?php

class Wp_Tagcloud_Public {

    private $option_name = 'wp_tagcloud';

    private $plugin_name;

    private $version;

    public function __construct( $plugin_name, $version, $options ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->options = $options;

    }

    public function enqueue_styles() {

        //wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', [], null, 'all' );
        //wp_enqueue_style( 'fonts-passion-one', 'https://fonts.googleapis.com/css?family=Passion+One', [], null, 'all' );
        //wp_enqueue_style( 'fonts-oxygen', 'https://fonts.googleapis.com/css?family=Oxygen', [], $this->version, 'all' );
        
        //wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css', ['parent-style'], $this->version, 'all' );
        
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-tagcloud-public.css', [], $this->version, 'all' );

    }

    public function enqueue_scripts() {
        if(is_page('submit')){
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-tagcloud-submit.js', [ 'jquery' ], $this->version, false );
        }
        if(is_front_page() || is_page('cloud')){
            wp_enqueue_script( 'webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js', [], null, false );
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-tagcloud-pack.js', [], $this->version, false );
            
        }

    }
    public function template_redirect() {
        if (is_page('submit') || is_page('cloud') || is_home) {
            set_query_var('tag_cloud_public', $this);
        }
    }

    public function post_count() {
        return wp_count_posts('cloudtags')->publish;
    }

    public function camel_case($str) {
        return preg_replace_callback('/_([a-z0-9])/', function($chr) {
            return strtoupper($chr[1]);
        }, $str);
    }

    public function get_toggle() {
         return get_option($this->option_name . '_toggle');
    }
    public function get_pricing() {
        $pricing = [];
        foreach ($this->options['fields_input'] as $key => $value) {
            if ($value[0] === null) {
                //null at ind 0 implies nested (sizing options)
                $pricing[$this->camel_case($key)] = [];
                foreach ($value as $key_1 => $value) {
                    $value = get_option($this->option_name . '_' . $key_1);
                    $pricing[$this->camel_case($key)][] = [$this->camel_case($key_1), (float) $value];
                }
                continue;
            }
            $value = get_option($this->option_name . '_' . $key);
            $pricing[$this->camel_case($key)] = (float) $value;
        }
        return $pricing;
    }

    public function create_post($slogan, $email, $premium = false, $url = false, $font = false, $font_bold = false, $font_size = false, $font_colour = false, $pp_data = false) {

        $post_id = -1;

        $author_id = 3;
        $slug = 'tag';
        $title = $slogan;

        /*if ($premium === false) {
            //select random standard options
            $font_options = array_keys($this->options['options_standard']['fonts']);
            $font_size_options = array_keys($this->options['options_standard']['font_sizes']);
            $font_colour_options = array_keys($this->options['options_standard']['font_colours']);
            $font = $font_options[rand(0, count($font_options) - 1)];
            $font_size = $font_size_options[rand(0, count($font_size_options) - 1)];
            $font_colour = $font_colour_options[rand(0, count($font_colour_options) - 1)];
            //use an integer for easier validation on the admin side
            $premium = 0;
            $url = '';
        }
        else {
            $premium = 1;
        }*/
        if ($premium === false) {
            $premium = 0;
            $pp_data = array_fill(0, 3, '');
        }
        else {
            $premium = 1;
        }
        if( get_page_by_title( $title ) === null ) {

            $post_id = wp_insert_post(
                [
                    'comment_status'    =>    'closed',
                    'ping_status'        =>    'closed',
                    'post_author'        =>    $author_id,
                    'post_name'        =>    $slug,
                    'post_title'        =>    $title,
                    'post_status'        =>    'publish',
                    'post_type'        =>    'cloudtags',
                    'meta_input' => [
                        'font' => $font,
                        'font_bold' => $font_bold,
                        'font_size' => $font_size,
                        'font_colour' => $font_colour,
                        'url' => $url,
                        'email' => $email,
                        'premium' => $premium,
                        'pp_id' => $pp_data[0],
                        'pp_email' => $pp_data[1],
                        'pp_data' => $pp_data[2]
                    ]
                ]
            );
            if ($post_id) {
                header('Location: /?thankyou');
                die();
            }
        }
        else {
            $post_id = -2;
        }
        //invalid input
        header('Location: /submit?invalid');
        die();
    }

    public function submit_form() {

        $font_options = array_keys($this->options['options_premium']['fonts']);
        $font_size_options = array_keys($this->options['options_premium']['font_sizes']);
        $font_colour_options = array_keys($this->options['options_premium']['font_colours']);

        $valid = true;
        if (!isset($_POST['email']) || !$email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            die('not a valid email address');
        }
        if (!isset($_POST['tag']) || !preg_match('/^[\x{0020}-\x{007f}\x{00A1}-\x{00FF}]{1,100}+$/u', $_POST['tag'], $tag)) {
            //valid latin-1 range (32 - 127, 161 - 255)
            die('not a valid slogan');
        }
        else {
            $tag = $tag[0];
        }

        $url = false;
        if (isset($_POST['url']) && $_POST['url'] !== '') {
            $url = filter_var($_POST['url'], FILTER_VALIDATE_URL);
            if (!$url) {
                die('not a valid url');
            }
        }

        $font = false;
        $font_bold = false;
        $font_size = false;
        $font_colour = false;
        
        if (isset($_POST['font_check_bold'])) {
            if ($_POST['font_check_bold'] === 'on') {
                $font_bold = true;
            }
            else {
                die('not a valid option');
            }
        }
        if (isset($_POST['url']) && in_array($_POST['font'], $font_options)) {
            $font = $_POST['font'];
        }
        if (isset($_POST['font']) && in_array($_POST['font'], $font_options)) {
            $font = $_POST['font'];
        }
        if (isset($_POST['font_size']) && in_array($_POST['font_size'], $font_size_options)) {
            $font_size = $_POST['font_size'];
        }
        if (isset($_POST['font_colour']) && in_array($_POST['font_colour'], $font_colour_options)) {
            $font_colour = $_POST['font_colour'];
        }

        $premium = false;
        $id = false;
        $pp_email = false;
        $pp_data = false;

        if (isset($_POST['pp_data']) && strlen(pp_data) < 2000) {
            //no documentation on max string length.  worth a crude check - potential DoS vector
            //due to processing/decoding JSON
            $premium = true;
            $data_json = json_decode(stripslashes($_POST['pp_data']), true);

            //no validation of pp_data at this stage.  do note that these could be printed admin side
            if (array_key_exists('id', $data_json)) {
                $id = $data_json['id'];
                if (array_key_exists('payer', $data_json) && array_key_exists('payer_info', $data_json['payer'])) {
                    $pp_email = $data_json['payer']['payer_info']['email'];
                }
                else {
                    die('invalid JSON data1');
                }
            }
            else {
                die('invalid JSON data2');
            }
            $pp_data = [$id, $pp_email, stripslashes($_POST['pp_data'])];
        }
        else {
            //just check that the premium toggle isn't set admin side
            if ($this->get_toggle() === '1') die('sorry, something went wrong');
        }

        $this->create_post($tag, $email, $premium, $url, $font, $font_bold, $font_size, $font_colour, $pp_data);

    }

    public function display_tag($title, $premium = 0, $font = 0, $bold = 0, $size = 0, $colour = 0, $url = false) {

        //remember this was unknown input
        $title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
        $title = mb_substr($title, 0, 20);
        $class =  'tag ';
        $class .= 'tag_font_' . $font . ' ';
        if ($bold) {
            $class .= 'tag_font_bold ';
        }
        $class .= 'tag_font_size_' . $size . ' ';
        /*if ($premium) {
            $class .= 'tag_font_colour_' . $colour;
        }
        else {
            $class .= 'random_c_' . $colour;
        }*/
        $class .= 'tag_font_colour_' . $colour;
        if ($url) {
        ?>
        <a href="<?php echo $url;?>" class="<?php echo $class;?>"><?php echo $title;?></a>
        <?php
        }
        else {
        ?>
        <span class="<?php echo $class;?>"><?php echo $title;?></span>
        <?php
        }
    }

    public function random_word($length = 6) {

        $string     = '';
        $vowels     = ['a','e','i','o','u'];  
        $consonants = [
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        ];  
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $string .= $consonants[rand(0,19)];
            $string .= $vowels[rand(0,4)];
        }
        return $string;
    }

    public function display_cloud() {

        if ($_GET['random']) {
            $i = 0;
            while ($i < 100) {
                $this->display_tag($this->random_word(rand(3, 16)), rand(0, 5), rand(0, 5), rand(0, 5));
                $i++;
            }
        }
        else {
            $posts = get_posts([
                'post_type' => 'cloudtags',
                'post_status' => 'publish',
                'numberposts' => -1
                //'order'    => 'ASC'
            ]);
            $tags = [];
            foreach ($posts as $post) {
                $meta = get_post_meta($post->ID, '', true);
                $tags[] = [
                    'title' => $post->post_title,
                    'font' => $meta['font'][0],
                    'font_bold' => $meta['font_bold'][0],
                    'font_size' => $meta['font_size'][0],
                    'colour' => $meta['font_colour'][0],
                    'url' => $meta['url'][0],
                    'premium' => $meta['premium'][0]
                ];
            }
            //few times for testing
            $i = 0;
            while($i < 10) {
            foreach ($tags as $key => $tag) {
                $this->display_tag($tag['title'], $tag['premium'], $tag['font'], $tag['font_bold'], $tag['font_size'], $tag['colour'], $tag['url']);
            }
            $i++;
            }
           
        }
    }
}
