<?php

class Wp_Tagcloud {

    protected $loader;

    protected $plugin_name;

    protected $version;


    //some of these values should be treated as placeholders
    //[title, type, step]
    public $options = [
        'fields_input' => [
            'price_base' => ['Base price (minimum)', 'number', '1'],
            'price_word' => ['Round price per word', 'number', '1'],
            'price_character' => ['Price per character', 'number', '.1'],
            'price_font' => ['Price for custom font', 'number', '1'],
            'price_bold' => ['Price for bold', 'number', '1'],
            'price_sizes' => [
                'price_size_120' => ['Font size 120%', 'number'],
                'price_size_140' => ['Font size 140%', 'number'],
                'price_size_160' => ['Font size 160%', 'number'],
                'price_size_180' => ['Font size 180%', 'number'],
                'price_size_200' => ['Font size 200%', 'number']
            ],
            'price_colour' => ['Price for custom colour', 'number', '1']
        ],
        'options_premium' => [
            'fonts' => [
                '1' => ['Asap-Bold.ttf', 'Asap Bold'],
                '2' => ['Cabin-Bold.ttf', 'Cabin Bold'],
                '3' => ['Exo-Bold.ttf', 'Exo Bold'],
                '4' => ['Gudea-Bold.ttf', 'Gudea Bold'],
                '5' => ['Magra-Bold.ttf', 'Magra Bold']
            ],
            'font_sizes' => [
                '0' => '100%',
                '1' => '120%',
                '2' => '140%',
                '3' => '160%',
                '4' => '180%',
                '5' => '200%'
            ],
            'font_colours' => [
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5'
            ]
        ],
        'options_standard' => [
            'fonts' => [
                '1' => ['Asap-Bold.ttf', 'Asap Bold'],
                '2' => ['Cabin-Bold.ttf', 'Cabin Bold'],
                '3' => ['Exo-Bold.ttf', 'Exo Bold'],
                '4' => ['Gudea-Bold.ttf', 'Gudea Bold'],
                '5' => ['Magra-Bold.ttf', 'Magra Bold']
            ],
            'font_sizes' => [
                '0' => '100%',
                '1' => '120%',
                '2' => '140%',
                '3' => '160%',
                '4' => '180%',
                '5' => '200%'
            ],
            'font_colours' => [
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7'
            ]
        ]
    ];

    public function __construct() {
        if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
            $this->version = PLUGIN_NAME_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'wp-tagcloud';

        $this->load_dependencies();

        $this->define_admin_hooks();
        $this->define_public_hooks();

    }

    private function load_dependencies() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-tagcloud-loader.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-tagcloud-admin.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wp-tagcloud-public.php';

        $this->loader = new Wp_Tagcloud_Loader();

    }

    private function define_admin_hooks() {

        $plugin_admin = new Wp_Tagcloud_Admin( $this->get_plugin_name(), $this->get_version(), $this->options );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_options_page' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'add_settings_section' );
        //$this->loader->add_action( 'update_option', $plugin_admin, 'update_option', 10, 3 );
        $this->loader->add_filter( 'pre_update_option', $plugin_admin ,'pre_update_option', 10, 3);

        $this->loader->add_action( 'init', $plugin_admin, 'create_posttype' );
        $this->loader->add_action( 'save_post', $plugin_admin, 'save_post', 10, 3 );
    }

    private function define_public_hooks() {

        $plugin_public = new Wp_Tagcloud_Public( $this->get_plugin_name(), $this->get_version(), $this->options );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

        $this->loader->add_action( 'admin_post_nopriv_process_form', $plugin_public, 'submit_form' );
        $this->loader->add_action( 'admin_post_process_form', $plugin_public, 'submit_form' );

        $this->loader->add_action( 'template_redirect', $plugin_public, 'template_redirect' );
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }

}
