<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes the dependencies used by the plugin,
 * registers the activation and deactivation functions and defines a function
 * that starts the plugin.
 *
 * @link              ''
 * @since             1.0.0
 * @package           Wp_Tagcloud
 *
 * @wordpress-plugin
 * Plugin Name:       tag cloud
 * Plugin URI:        /tagcloud
 * Description:       Cloud Tags.
 * Version:           1.0.0
 * Author:            ''
 * Author URI:        ''
 * License:           
 * License URI:       
 * Text Domain:       wp-tagcloud
 * Domain Path:       
 */


if ( ! defined( 'WPINC' ) ) {
    die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.0' );

function activate_wp_tagcloud() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-tagcloud-activator.php';
    Wp_Tagcloud_Activator::activate();
}

function deactivate_wp_tagcloud() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-tagcloud-deactivator.php';
    Wp_Tagcloud_Deactivator::deactivate();
}

/*register_activation_hook( __FILE__, 'activate_wp_tagcloud' );
register_deactivation_hook( __FILE__, 'deactivate_wp_tagcloud' );*/

require plugin_dir_path( __FILE__ ) . 'includes/class-wp-tagcloud.php';

function run_wp_tagcloud() {

    $plugin = new Wp_Tagcloud();
    $plugin->run();

}
run_wp_tagcloud();
