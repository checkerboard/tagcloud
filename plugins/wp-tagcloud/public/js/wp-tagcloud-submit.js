function tagSubscriptionForm(form, pricing, premium, ppActions, setTransaction) {

    var cloud = true;
    var transaction = {};
    var previewSlogan;
    var tagPreviewClass = 'tag tag-preview preview-slogan';
    if (cloud === true) {
        tagPreviewClass = 'preview-slogan';
    }

    var sloganPreview = document.querySelector('#slogan-preview');
    /*var optionsPremium = document.querySelector('#options-premium');
    var submitButton = document.querySelector('#tag-submit');
    var paypalButton = document.querySelector('#tag-paypal-button');
    var topMenu = document.querySelector('#top-menu');*/

    var inputSloganCount = document.querySelector('.input-slogan-count');

    var inputTag = form.querySelector('#tag');
    //var selectFont = form.querySelector('#font');
    var selectFontSize = form.querySelector('#font_size');
    var selectColour = form.querySelector('#font_colour');

    var checkFontBold = form.querySelector('#font_check_bold');
    
    var elePrice = form.querySelector('#price');

    var demoSlogan = 'Your slogan';
    var demoSlogans = [
        [0, 'conjectural'],
        [0, 'trial'],
        [0, 'preview'],
        [0, 'contingent'],
        [0, 'demo'],
        [0, 'indefinite'],
        [0, 'not final'],
        [0, 'preliminary'],
        [0, 'exploratory']
    ];
    var enteredSlogan;

    //retrieve select options from html elements
    var fontOptions = [], fontSizeOptions = [], fontColourOptions = [];
    function getSelectOptions(eleSelect, ary) {
        forEachNl(eleSelect.querySelectorAll('option'), function(ele) {
            ary.push([ele.value, ele.getAttribute('data-value')]);
        });
    }
    //getSelectOptions(selectFont, fontOptions);
    getSelectOptions(selectFontSize, fontSizeOptions);
    getSelectOptions(selectColour, fontColourOptions);

    shuffleArray(fontOptions, .5);
    shuffleArray(fontSizeOptions, .6);

    if (premium === true) {
        ppActions.disable();
    }

    function calcPrice() {
        
        if (premium === false) return;
        var price = pricing.priceBase;
        /*if (pricing.perWord === true) {
            var wordCount = inputTag.value.match(/\w+/g);
            if (wordCount && wordCount.length > 1) {
                price += wordCount.length - 1;
            }
        }*/

        price += Math.floor(inputTag.value.length * pricing.priceCharacter);

        if (checkFontBold.checked === true) {
            //select from an array of font sizes
            price += pricing.priceBold;
        }

        if (selectFontSize.value != 0) {
            //select from an array of font sizes
            price += pricing.priceSizes[selectFontSize.value - 1][1];
        }
        
        /*if (selectFont.value != 1) {
            //any font that isn't the default
            price += pricing.priceFont;
        }*/
        if (selectColour.value != 0) {
            //any colour that isn't the default
            price += pricing.priceColour;
        }
        elePrice.innerHTML = '$' + price;
        transaction = setTransaction(price);
        console.log(transaction);
    }
    function ppCheckValid() {
        //paypal requires 'real-time' validation
        //we're calling this with every input 'input'
        if (form.checkValidity() === false) {
            if (premium === true) {
                ppActions.disable();
            }
            return false;
        }
        if (premium === true) {
            ppActions.enable();
        }
    }
    function forEachNl(nl, cb) {
        //for nodelist compatibility in old browsers
        for (var i = 0; i < nl.length; i++) {
            cb(nl[i], i);
        }
    }
    function shuffleArray(ary) {
        var seed = Math.random();
        ary.forEach(function(item, i) {
            var j = Math.floor(seed * (i + 1));
            var temp = ary[i];
            ary[i] = ary[j];
            ary[j] = temp;
        });
    }
    function removeClassSet(opts, ele, classPrefix) {
        //this is getting quite ad hoc, but what can you do
        opts.forEach(function(item) {
            ele.classList.remove(classPrefix + item[0]);
        });
    }
    function createPreview(slogans) {
        sloganPreview.innerHTML = '';
        slogans.forEach(function(item, i) {
            var node = document.createElement('span');
            if(item[0] === 1) {
                if (previewSlogan) {
                    node = previewSlogan.cloneNode(true);
                }
                else {
                    node.className = tagPreviewClass;
                }
                previewSlogan = node;
            }
            else {
                node.className = 'random_c_' + fontColourOptions[i % fontColourOptions.length][0] +
                ' tag_font_size_' + fontSizeOptions[i % fontSizeOptions.length][0];
            }
            node.innerHTML = item[1] + ' ';
            sloganPreview.appendChild(node);
        });
    }
    function setOption(val, classPrefix) {
        return classPrefix + val;
    }
    function setPreview(slogan) {
        enteredSlogan = slogan;
        if (!slogan) slogan = demoSlogan;

        if (cloud === true) {
            var copySlogans = demoSlogans.slice(0);
            copySlogans.unshift([1, slogan]);
            createPreview(copySlogans);
        }
        else {
            createPreview([[1, slogan]]);
        }
    }
    return {
        init: function() {
            this.events();
            setPreview(demoSlogan);
            calcPrice();
            //we need paypal's active object before we can assign event handlers
            //hide form until ready
            form.classList.remove('form-hide');
        },
        events: function() {
            form.addEventListener('submit', function(e) {
                if (form.checkValidity() === false) {
                    e.preventDefault();
                    e.stopPropagation();
                    form.classList.add('was-validated');
                    return false;
                }
                else if (premium === true) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
            form.addEventListener('input', function(e) {
                if (e.target.matches('input')) {
                    ppCheckValid();
                }
            }, false);
            sloganPreview.addEventListener('click', function() {
                if (cloud === false) return true;
                this.shufflePreview();
            }.bind(this), false);
            /*form.querySelector('#premium').addEventListener('change', function() {
                form.querySelector('#font').value = fontOptions[0];
                form.querySelector('#font_size').value = fontSizeOptions[0];
                form.querySelector('#font_colour').value = fontColourOptions[0];
                if(this.checked) {
                    optionsPremium.classList.remove('options-hide');
                    submitButton.classList.remove('submit-show');
                    paypalButton.classList.add('paypal-show');
                }
                else {
                    removeClassSet(fontOptions, previewSlogan, 'tag_font_');
                    removeClassSet(fontSizeOptions, previewSlogan, 'tag_font_size_');
                    removeClassSet(fontSizeOptions, previewSlogan, 'tag_font_colour_');
                    optionsPremium.classList.add('options-hide');
                    submitButton.classList.add('submit-show');
                    paypalButton.classList.remove('paypal-show');
                }
            }, false);*/

            inputTag.addEventListener('input', function() {
                if (this.validity.valid === false && this.value) {
                    this.classList.add('invalid');
                    return false;
                }
                this.classList.remove('invalid');
                var maxLength = parseInt(this.getAttribute('maxlength'));
                var currentLength = this.value.length; 
                var remaining = (maxLength - currentLength);
                inputSloganCount.innerHTML = remaining;
                setPreview(this.value);
                calcPrice();

            }, true);
            /*selectFont.addEventListener('change', function() {
                var className = setOption(this.value, 'tag_font_');
                removeClassSet(fontOptions, previewSlogan, 'tag_font_');
                previewSlogan.classList.add(className);
                calcPrice();
            }, false);*/
            checkFontBold.addEventListener('change', function() {
                previewSlogan.classList.toggle('tag_font_bold');
                calcPrice();
            }, false);
            selectFontSize.addEventListener('change', function() {
                var className = setOption(this.value, 'tag_font_size_');
                removeClassSet(fontSizeOptions, previewSlogan, 'tag_font_size_');
                previewSlogan.classList.add(className);
                calcPrice();
            }, false);
            /*form.querySelector('#font_colour').addEventListener('change', function() {
                var className = setOption(this, 'tag_font_colour_');
                removeClassSet(fontSizeOptions, previewSlogan, 'tag_font_colour_');
                previewSlogan.classList.add(className);
            }, false);*/
            form.querySelector('#swatch-container').addEventListener('click', function(e) {
                if (e.target.matches('.swatch')) {
                    //ie 10 doesn't support dataset
                    var key = e.target.getAttribute('data-key');
                    var colour = e.target.getAttribute('data-colour');
                    var className = setOption(colour, 'tag_font_colour_');
                    selectColour.value = key;
                    removeClassSet(fontColourOptions, previewSlogan, 'tag_font_colour_');
                    previewSlogan.classList.add(className);
                    calcPrice();
                }
            }, false);
        },
        shufflePreview: function() {
            var copySlogans = demoSlogans.slice(0);

            var slogan = enteredSlogan;
            if (!slogan) slogan = demoSlogan;

            copySlogans.unshift([1, slogan]);
            shuffleArray(copySlogans);
            createPreview(copySlogans);
        },
        ppAddValidation: function() {
            //this displays the validation messages, nothing else
            //IE doesn't support multiple arguments for classList.add
            form.classList.add('needs-validation');
            form.classList.add('was-validated');
        },
        payment: function(data, actions) {
            console.log(transaction);
            return actions.payment.create(transaction);
        },
        submitForm: function(ppData) {
            //we could use xhr and formData, but it's all good
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'pp_data';
            input.value = ppData;
            form.appendChild(input);
            form.submit();
        }
    };
}

function registerPaypal(pricing) {
    var soldCount = document.querySelector('#sold_count');
    var counterLink = document.createElement('span');
    counterLink.innerHTML = 'Sold: ' + postCount + ' Available: 1000';
    soldCount.appendChild(counterLink);

    var form = document.querySelector('#register_tag');

    var setTransaction = function(total) {
        return {
            application_context: {
                shipping_preference: 'NO_SHIPPING'
            },
            note_to_payer: 'Contact us with any queries regarding your order.',
            transactions: [{
                amount: {
                    total: total,
                    currency: 'USD'
                },
                payment_options: {
                    allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
                },
                item_list: {
                    items: [
                        {
                        name: 'Slogan',
                        quantity: '1',
                        price: total,
                        currency: 'USD'
                        }
                    ]
                }
            }]
        };
    };
    paypal.Button.render({
        env: 'sandbox',
        client: {
            sandbox: 'AV2w7VO7j_t3L_yhtSiqFKkyPAU_XRQd8s_w3cBTn8cfHqs_Nqw0IKlgpyW-UzrtsIQN0qMhjTx929BC',
            production: 'demo_production_client_id'
        },
        
        locale: 'en_US',
        style: {
            size: 'medium',
            color: 'blue',
            shape: 'rect',
        },
        validate: function(actions) {
            //this is triggered once on initial paypal render
            //we need to pass the actions object to our various event handlers
            subscription = tagSubscriptionForm(form, pricing, true, actions, setTransaction);
            subscription.init();
        },
        onClick: function() {
            subscription.ppAddValidation();
        }, 
        payment: function(data, actions) {
            return subscription.payment(data, actions);
        },
        onAuthorize: function (data, actions) {
            return actions.payment.execute()
            .then(function (response) {
                var jsonResponse = JSON.stringify(response);
                subscription.submitForm(jsonResponse);
                //form.submit();
            });
        }
    }, '#tag-paypal-button');
}


window.onload = function() {
    if (premium) {
        registerPaypal(pricing);
    }
    else {
        var form = document.querySelector('#register_tag');
        subscription = tagSubscriptionForm(form, {}, false);
        subscription.init();
    }
};