
function packing(items, cb) {
    //pass in an array [[ele, width, height]]

    var containerHeight = 0;

    var mWidth;
    var mHeight = 10000;
    var cellSize = 7;

    var populated;

    function scanCells(x, y, w, h) {

        var row = y * mWidth;

        var rowBeginning = row + x;
        var rowEnd;
        var k = 0;
        for (k; k < h; k++, y++) {
            rowEnd = rowBeginning + w;
            var l = rowBeginning;
            var m = 0;
            for (l; l < rowEnd; l++, m++) {
                if (populated[l] !== 0) {
                    return false;
                }
            }
            rowBeginning = (y * mWidth) + x;
        }
        return true;
    }

    function populateCells(x, y, w, h) {

        var row = y * mWidth;
        var rowBeginning = row + x;
        var rowEnd;
        var k = 0;
        for (k; k < h; k++, y++) {
            rowEnd = rowBeginning + w;
            var l = rowBeginning;
            var m = 0;
            for (l; l < rowEnd; l++, m++) {
                populated[l] = 1;
            }
            rowBeginning = (y * mWidth) + x;
        }

    }

    return {
        place: function(width) {
            containerHeight = 0;
            mWidth = width;
            populated = new Uint8Array(mWidth * mHeight);

            function scan(n) {

                var scanHeight = mHeight;

                var w = items[n][1];
                var h = items[n][2];
                if (w > mWidth - cellSize) {
                    //consider large items restricted to max width and place anyway
                    w = mWidth - cellSize;
                }
                var i = 0, j;
                for (i; i < scanHeight; i += cellSize) {

                    j = 0;
                    for (j; j < mWidth - w; j += cellSize) {

                        if (scanCells(j, i, w, h)) {
                            if (i + h > containerHeight) containerHeight = i + h;
                            cb(n, j, i, containerHeight);
                            
                            populateCells(j, i, w, h);

                            if (n < items.length - 1) {
                                n++;
                                scan(n);
                                return;
                            }
                            end = new Date();
                            time = end.getTime() - start.getTime();
                            console.log(time);
                            return;
                        }

                    }
                }
            }
            scan(0);
        }
    };
}


var start, end, time;
start = new Date();



function resizeThrottle(cb) {
    var running = false;

    function throttle() {
        cb();
        running = false;
    }

    function resize() {
        if (!running) {
            running = true;
            setTimeout(throttle, 300);
        }
    }
    window.addEventListener('resize', resize);
}


function launch() {
    function forEachNl(nl, cb) {
        //for nodelist compatibility in old browsers
        for (var i = 0; i < nl.length; i++) {
            cb(nl[i], i);
        }
    }
    var parentEle = document.getElementById('wp_cloud');
    var items = [];
    forEachNl(parentEle.querySelectorAll('.tag'), function(ele, i) {
        //console.log(window.getComputedStyle(ele, null).getPropertyValue('font-size'));
        /*var size = window.getComputedStyle(ele, null).getPropertyValue('font-size').slice(0, -2);
        var color = window.getComputedStyle(ele, null).getPropertyValue('color');
        var weight = window.getComputedStyle(ele, null).getPropertyValue('font-weight');*/
        //if (size > 34) size = 34;
        items.push([ele, ele.offsetWidth, ele.offsetHeight]);
    
    });
    
    function shuffleArray(ary) {
        seed = Math.random();
        ary.forEach(function(item, i) {
            var j = Math.floor(seed * (i + 1));
            var temp = ary[i];
            ary[i] = ary[j];
            ary[j] = temp;
        });
    }
    
    shuffleArray(items);
        
    var pack = packing(items, function(ind, x, y, containerHeight) {
        items[ind][0].style.left = x + 'px';
        items[ind][0].style.top = y + 'px';
        wpCloud.style.height = containerHeight + 'px';
        wpCloud.style.visibility = 'visible';
    });
    
    var wpCloud = document.getElementById('wp_cloud');
    pack.place(wpCloud.clientWidth);
    resizeThrottle(function() {
        start = new Date();
        pack.place(wpCloud.clientWidth);
    });
}

WebFont.load({
    custom: {
        families: ['font_2']
    },
    active: launch
});
